public class ToStringTreeVisitor implements Visitor<Tree, String> {

    public String visit(Tree tree) {
        String str = tree.getId() + ", ";

        // visit all child nodes
        for (Tree child : tree.getChildren()) 
            str += child.accept(this);

        return str;
    }
}
