import java.util.Set;
import java.util.HashSet;
public class Tree implements Visitable<Tree> {
    private Integer id;
    private Set<Tree> children = new HashSet<Tree>();

    public Tree(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public Set<Tree> getChildren() {
        return children;
    }

    public <R> R accept(Visitor<Tree, R> visitor) {
        return visitor.visit(this);
    }
}

interface Visitable<V> {
    public <R> R accept(Visitor<V, R> visitor);
}

interface Visitor<V, R> {
    public R visit(V v);
}
