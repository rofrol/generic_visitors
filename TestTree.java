// http://www.pointyspoon.com/2010/10/generic-visitors/
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;

public class TestTree {
  public static void main( String args[] ) {
    Tree root = new Tree(1);
    root.getChildren().add(new Tree(2));
    root.getChildren().add(new Tree(3));

    String str = root.accept(new ToStringTreeVisitor());
    System.out.println(str); // returns => "1, 2, 3"
  }
}
